package services;

import junit.framework.Assert;
import org.junit.Test;

/**
 * User: cgatay
 * Date: 02/12/12
 * Time: 16:47
 */
public class AuthenticationServiceTest {
    @Test
    public void testLoginCGatay_true(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertEquals("success", service.login("cgatay", "M6cga"));
    }

    @Test
    public void testLoginMOdersky_true(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertEquals("success", service.login("modersky", "M8mod"));
    }

    @Test
    public void testLoginAlexisMP_true(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertEquals("success", service.login("alexismoussinepouchkine", "M0ale"));
    }

    @Test
    public void testLoginCGatay_false(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertNull("success", service.login("cgatay", "password"));
    }

    @Test
    public void testLoginOK_True(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertEquals("success", service.oldLogin("Ma3Zr"));
    }

    @Test
    public void testLoginDeuxChiffres_True(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertEquals("success", service.oldLogin("Ma3r5"));
    }

    @Test
    public void testLoginTropCourt_False(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertNull(service.oldLogin("Ma3r"));
    }

    @Test
    public void testLoginTropLong_False(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertNull(service.oldLogin("Ma3rAwxD"));
    }

    @Test
    public void testLoginSansChiffre_False(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertNull(service.oldLogin("MarAw"));
    }
    @Test
    public void testLoginSansMAuDebut_False(){
        final AuthenticationService service = new AuthenticationService();
        Assert.assertNull(service.oldLogin("m3rAw"));
    }
}
