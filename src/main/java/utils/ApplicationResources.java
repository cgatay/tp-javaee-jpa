package utils;

import javax.annotation.sql.DataSourceDefinition;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * User: cgatay
 * Date: 09/12/12
 * Time: 12:57
 */
@DataSourceDefinition(name="java:app/jdbc/tpjavaee",
                      className="com.mysql.jdbc.jdbc2.optional.MysqlDataSource",
                      user="tpjavaee",
                      password="tpjavaee",
                      databaseName="tpjavaee",
                      serverName="localhost",
                      portNumber=3306 )
public class ApplicationResources {
    /**
     * Query String to allow clean redirect by JSF.
     */
    public static final String FACES_REDIRECT = "?faces-redirect=true";

    @SuppressWarnings("unused")
    @Produces
    @PersistenceContext
    private EntityManager em;

    /**
     * @return the current faces context
     */
    @Produces
    @RequestScoped
    public FacesContext getFacesContext(){
        return FacesContext.getCurrentInstance();
    }
}
