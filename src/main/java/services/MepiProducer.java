package services;

import models.Mepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * User: cgatay
 * Date: 08/12/12
 * Time: 17:56
 */
@ApplicationScoped
public class MepiProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MepiProducer.class);
    @Inject
    private EntityManager em;
    private List<Mepo> mepi;

    @PostConstruct
    @SuppressWarnings("unchecked")
    public void initList() {
        mepi = em.createQuery("SELECT m FROM Mepo m ORDER BY m.createdAt DESC").getResultList();
    }

    /**
     * Observer method called whenever a mepo is inserted.
     * @param newMepo mepo to insert
     */
    public void onMepiInserted(@Observes(notifyObserver = Reception.IF_EXISTS) Mepo newMepo){
        LOGGER.info("Caught event, reloading list from database");
        initList();
    }

    /**
     * This method returns the list of the available mepi.
     * It is annotated with the @Produces annotation to tell that it generates values
     * It is annotated with the @Named annotation to be able to call it by name in mepi.jsf
     * @return list of mepi.
     */
    @Produces
    @Named
    public List<Mepo> loadMepi() {
        return mepi;
    }

}
