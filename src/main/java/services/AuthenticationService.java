package services;

import models.UserInfo;
import utils.ApplicationResources;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * User: cgatay
 * Date: 02/12/12
 * Time: 16:45
 */
@Named
@ApplicationScoped
public class AuthenticationService implements Serializable {

    @Inject
    FacesContext facesContext;

    /**
     * Login method checking if the supplied userInfo matches our condition on its password/nickname combination :
     *  * 5 chars long
     *  * starting with a capital M
     *  * second char is the length of the nickName (0 if it exceeds 9 chars)
     *  * the three first chars of the nickname
     *
     * For example :
     *  * cgatay > M6cga
     *  * modersky > M8mod
     *
     *  As a side effect, it sets the flag userInfo#loggedIn with the result of the check
     *
     * @param userInfo userInfo to check
     * @return next view to show / null if a refresh is needed
     */
    public String login(final UserInfo userInfo){
        final String login = login(userInfo.getNickName(), userInfo.getPassword());
        userInfo.setLoggedIn(login != null);
        if (login == null){
            FacesContext.getCurrentInstance().addMessage("Invalid credentials",
                                                         new FacesMessage("Invalid credentials !"));
        }
        return login;
    }

    /**
     * Method to invalidate current user session. It relies on invalidating user's session from FacesContext
     * @return next path (with redirect)
     */
    public String logout(){
        facesContext.getExternalContext().invalidateSession();
        return "mepi" + ApplicationResources.FACES_REDIRECT;
    }

    /**
     * Login method checking if the supplied password matches our condition :
     *  * 5 chars long
     *  * starting with a capital M
     *  * second char is the length of the nickName (0 if it exceeds 9 chars)
     *  * the three first chars of the nickname
     *
     * For example :
     *  * cgatay > M6cga
     *  * modersky > M8mod
     *
     * @param nickName nickName to identify
     * @param password password to check
     * @return next view to show / null if a refresh is needed
     */
    String login(final String nickName, final String password) {
        boolean validLogin = false;
        if (nickName != null && password != null && nickName.length() > 3) {
            int nickNameLength = nickName.length() > 9 ? 0 : nickName.length();
            validLogin = password.length() == 5 && password.equals("M"+nickNameLength+nickName.substring(0,3));
        }
        return validLogin ? "success" : null;
    }

    /**
     * Login method corresponding to the first implementation, without password
     * We only accept 5 chars nickNames matching the following regex : M.*[0-9]+.*
     * starts with a capital M and containing at least a digit.
     * @param nickName nickName to check
     * @return next view to show, null if a refresh is needed
     */
    String oldLogin(final String nickName) {
        boolean validLogin = false;
        if (nickName != null) {
            validLogin = nickName.length() == 5 && nickName.matches("M.*[0-9]+.*");
        }
        return validLogin ? "success" : null;
    }
}
